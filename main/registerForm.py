from django import forms

class UserForm(forms.Form):
    name = forms.CharField(label='Name', max_length=100)
    email = forms.EmailField(label='Email')
    phone = forms.IntegerField(label='Phone')
    password = forms.CharField(label='Password')
    confirm_password = forms.CharField(label='Confirm Password')