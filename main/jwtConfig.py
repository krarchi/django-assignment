import jwt

secret = 'aserfxcscsddffdfgfghghgbvbvbbcv'

def createToken(email):
    token = jwt.encode({'email': email},secret,algorithm='HS256').decode('utf-8')
    return token

def decodeToken(token):
    try:
        data = jwt.decode(token,secret,algorithms='HS256')
        return data
    except:
        return False    