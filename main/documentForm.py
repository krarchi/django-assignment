from django import forms

class DocumentForm(forms.Form):
    name = forms.CharField(label= 'Name',max_length=100)
    description =  forms.CharField(label='Description')
    document = forms.FileField()