from django.shortcuts import render, redirect
from django.http import HttpResponse, FileResponse
from django.template import loader
from .registerForm import UserForm
from .loginForm import LoginForm
from .documentForm import DocumentForm
from .models import Users, Document
from django.contrib import messages
from django.contrib.auth.hashers import make_password, check_password
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from .jwtConfig import decodeToken, createToken


def index(request):
    print (request)
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            if Users.objects.filter(email= email).exists():
                user = Users.objects.filter(email= email).first()
                flag = check_password(password, user.password)
                if flag:
                    token = createToken(email=email)
                    return redirect('./home/'+token)
                    
                else:
                    messages.warning(request,'Wrong Credentials! Please retry.')
            else:
                messages.warning(request,'User Not Found! Please Sign Up.')
    else:
        form = LoginForm()
    return render(request, 'index.html', {'form':  form,'name':'Asdsasdas'})    

def signup(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            phone = form.cleaned_data['phone']
            password = make_password(form.cleaned_data['password'])
            if Users.objects.filter(email= email).exists():
                messages.warning(request, 'Email is already Taken')
                template = loader.get_template('signup.html')
                return HttpResponse(template.render({'name': name,'phone': phone, 'email': email},request))
            elif Users.objects.filter(phone= phone).exists():
                messages.warning(request, 'Phone Number is already Taken')
                template = loader.get_template('signup.html')
                return HttpResponse(template.render({'name': name,'phone': phone, 'email': email},request))
            else:
                user = Users(name=name,email=email,phone=phone,password=password)
                user.save()
                current_site = get_current_site(request)
                mail_subject = 'Activate your account.'
                token = createToken(email=email)
                message = render_to_string('email.html', {
                'user': user,
                'domain': current_site.domain,
                'token' : token
                })
                to_email = form.cleaned_data.get('email')
                email = EmailMessage(
                mail_subject, message, to=[to_email]
                )
                email.send()
                messages.success(request,'Signed Up successfully! Please verify your email.')
    else:
        form = UserForm()
    return render(request, 'signup.html', {'form': form})

def home(request,token):
    decodedData = decodeToken(token)
    if decodedData:
        userEmail = decodedData['email']
        form = DocumentForm(request.POST, request.FILES)
        user = Users.objects.filter(email=userEmail).first()
        if user.verified:
            if request.method == 'POST':
                if form.is_valid():
                    name = form.cleaned_data['name']
                    description = form.cleaned_data['description']
                    doc = Document(name=name,description=description,document= request.FILES['document'], user= user)
                    doc.save()
                    doc_data = user.document_set.all()
                    data = {'user': user.name, 'verified': True, 'doc' : doc_data, 'form': form}
            else:
                doc_data = user.document_set.all()
                form = DocumentForm()
                data = {'user': user.name, 'verified': True, 'doc' : doc_data, 'form': form}
        else:
            data = {'user': user.name, 'verified': False}
        return render(request, 'home.html', data)
    else:
        messages.warning(request, 'Invalid Token!')
        return redirect('/')




def activate(request, token):
    decodedData = decodeToken(token)
    if decodedData:
        userEmail = decodedData['email']
        user = Users.objects.filter(email=userEmail).first()
        user.verified = True
        user.save()
        messages.success(request, 'Email verified! Please Login to continue.')
    else:
        messages.warning(request, 'Invalid Token!')
    return redirect('/')
    

def download(request, id):
    obj = Document.objects.get(id=id)
    filename = obj.document.path
    response = FileResponse(open(filename, 'rb'))
    return response

