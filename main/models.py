from django.db import models

# Create your models here.
class Users(models.Model):
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    phone = models.IntegerField()
    password = models.CharField(max_length=100)
    verified = models.BooleanField(default= False)
    
    def __str__(self):
        return self.name

class Document(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    document = models.FileField(upload_to='documents/')
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
