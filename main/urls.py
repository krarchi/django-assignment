from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('signup', views.signup, name='signup'),
    path('home/<token>/', views.home, name='home'),
    path('activate/<token>/',views.activate, name='activate'),
    path('download/<id>/', views.download, name='download')
]