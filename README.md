# Instructions

Run the following commands in root directory of project.

```bash
pip install -r requirements.txt
```
```bash

python manage.py makemigrations

python manage.py migrate

```

To Start the server run the following command in root directory of project

```bash

python manage.py runserver

```

## Developed on 

Python 3.6.9

## Database Engine

SQLite

## Access Admin Panel

<baseurl>/admin